﻿using Moq;
using NUnit.Framework;
using PuzzleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleTests
{
    [TestFixture]
    public class ResolverTests
    {
        [Test]
        public void SolveTest()
        {
            var puzzleRules = new PuzzleRules();
            var resolver = new Resolver(puzzleRules);
            resolver.InitGraph(true);

            CollectionAssert.AreEqual(resolver.Solve(new int[] { 1, 2, 3, 4, 6, 5, 0, 7, 8, 9 }), new int[] { 6, 4 });
            CollectionAssert.AreEqual(resolver.Solve(new int[] { 1, 2, 3, 4, 6, 5, 8, 9, 7, 0 }), new int[] { 9, 7, 8, 6, 4 });
            CollectionAssert.AreEqual(resolver.Solve(new int[] { 1, 2, 5, 3, 4, 7, 6, 9, 8, 0 }), new int[] { 9, 7, 5, 3 });
        }

        [Test]
        public void CreateGraphTest()
        {
            var puzzleRules = new Mock<IPuzzleRules>();
            puzzleRules.Setup(pr => pr.TargetPosition).Returns("00");
            puzzleRules.Setup(pr => pr.GetPossibleMoves(It.Is<string>(position => position == "00"))).Returns(new char[] { '1' });
            puzzleRules.Setup(pr => pr.GetPossibleMoves(It.Is<string>(position => position == "11"))).Returns(new char[] { '1', '2', '3' });
            puzzleRules.Setup(pr => pr.GetPossibleMoves(It.Is<string>(position => position == "22"))).Returns(new char[] { '2' });
            puzzleRules.Setup(pr => pr.GetPossibleMoves(It.Is<string>(position => position == "33"))).Returns(new char[] { '3' });

            puzzleRules.Setup(pr => pr.GetNewPosition(It.Is<string>(position => position == "00"), It.Is<char>(move => move == '1'))).Returns("11");
            puzzleRules.Setup(pr => pr.GetNewPosition(It.Is<string>(position => position == "11"), It.Is<char>(move => move == '1'))).Returns("00");
            puzzleRules.Setup(pr => pr.GetNewPosition(It.Is<string>(position => position == "11"), It.Is<char>(move => move == '2'))).Returns("22");
            puzzleRules.Setup(pr => pr.GetNewPosition(It.Is<string>(position => position == "11"), It.Is<char>(move => move == '3'))).Returns("33");
            puzzleRules.Setup(pr => pr.GetNewPosition(It.Is<string>(position => position == "22"), It.Is<char>(move => move == '2'))).Returns("11");
            puzzleRules.Setup(pr => pr.GetNewPosition(It.Is<string>(position => position == "33"), It.Is<char>(move => move == '3'))).Returns("11");

            var resolver = new Resolver(puzzleRules.Object);
            resolver.InitGraph(false);
            var graph = resolver.GetGraph();
            var expectedGraph = new Dictionary<string, Node>()
            {
                { "00", new Node(0, '0')},
                { "11", new Node(1, '1')},
                { "22", new Node(2, '2')},
                { "33", new Node(2, '3')}
            };
            CollectionAssert.AreEquivalent(graph, expectedGraph);

        }
    }
}
