﻿using NUnit.Framework;
using PuzzleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleTests
{
    [TestFixture]
    public class UserInputProcessorTests
    {
        [Test]
        public void InputIsValidTests()
        {
            var processor = new UserInputProcessor();
            Assert.IsTrue(processor.InputIsValid("0123456789"));
            Assert.IsTrue(processor.InputIsValid("4231089576"));
            Assert.IsFalse(processor.InputIsValid(""));
            Assert.IsFalse(processor.InputIsValid("01234a6789"));
            Assert.IsFalse(processor.InputIsValid("01234"));
            Assert.IsFalse(processor.InputIsValid("0123456709"));
            Assert.IsFalse(processor.InputIsValid("01234567892"));
        }

        [Test]
        public void ConvertInputTests()
        {
            var processor = new UserInputProcessor();
            CollectionAssert.AreEqual(processor.ConvertInput("0123456789"), new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
        }

        [Test]
        public void ConvertOutputTests()
        {
            var processor = new UserInputProcessor();
            Assert.AreEqual(processor.ConvertOutput(new int[] { 1, 5, 3, 2, 1, 2 }), "1; 5; 3; 2; 1; 2; ");

        }
    }
}
