﻿using NUnit.Framework;
using PuzzleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleTests
{
    [TestFixture]
    public class PuzzleRulesTests
    {
        [Test]
        public void GetPossibleMovesTest()
        {
            var puzzleRules = new PuzzleRules();
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("0123456789"), new char[] { '1', '2' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1023456789"), new char[] { '1', '4' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1203456789"), new char[] { '1', '3', '5' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1230456789"), new char[] { '3', '4' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1234056789"), new char[] { '2', '4', '6' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1234506789"), new char[] { '3', '7' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1234560789"), new char[] { '5', '8' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1234567089"), new char[] { '6', '8', '9' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1234567809"), new char[] { '7', '8', '9' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("1234567890"), new char[] { '8', '9' });
            CollectionAssert.AreEquivalent(puzzleRules.GetPossibleMoves("9870654321"), new char[] { '7', '6' });
        }

        [Test]
        public void GetNewPositionTest()
        {
            var puzzleRules = new PuzzleRules();
            Assert.AreEqual(puzzleRules.GetNewPosition("1230456789", '3'), "1203456789");
            Assert.AreEqual(puzzleRules.GetNewPosition("1230456789", '4'), "1234056789");
            Assert.AreEqual(puzzleRules.GetNewPosition("1234567089", '9'), "1234567980");
            Assert.AreEqual(puzzleRules.GetNewPosition("1203456789", '1'), "0213456789");
        }
    }
}
