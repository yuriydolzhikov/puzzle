﻿using Autofac;
using PuzzleEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var puzzleRules = new PuzzleRules();
            var resolver = new Resolver(puzzleRules);
            var userInputProcessor = new UserInputProcessor();
            resolver.InitGraph(true);

            while (true)
            {
                Console.Write("Enter start position (for example 0123456789): ");
                var input = Console.ReadLine();
                if (userInputProcessor.InputIsValid(input))
                {
                    var parameter = userInputProcessor.ConvertInput(input);
                    var solution = resolver.Solve(parameter);
                    Console.WriteLine(userInputProcessor.ConvertOutput(solution));
                }
                else
                {
                    Console.WriteLine("Input is not valid");
                }
            }
        }
    }
}
