﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleEngine
{
    public class UserInputProcessor : IUserInputProcessor
    {
        const int PositionSize = 10;

        public int[] ConvertInput(string input)
        {
            return input.Select(c => Convert.ToInt32(c.ToString())).ToArray();
        }

        public string ConvertOutput(int[] output)
        {
            var result = new StringBuilder(PositionSize);
            foreach (var n in output)
            {
                result.Append(n.ToString());
                result.Append("; ");
            }
            return result.ToString();
        }

        public bool InputIsValid(string input)
        {
            if (input.Length != PositionSize)
            {
                return false;
            }

            for (int i = 0; i < PositionSize; i++)
            {
                if (!input.Contains(i.ToString()))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
