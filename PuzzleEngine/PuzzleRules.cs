﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleEngine
{
    public class PuzzleRules : IPuzzleRules
    {
        private Dictionary<int, int[]> _edges = new Dictionary<int, int[]>
        {
            { 0, new int[] { 1, 2 } },
            { 1, new int[] { 0, 4 } },
            { 2, new int[] { 0, 3, 5 } },
            { 3, new int[] { 2, 4 } },
            { 4, new int[] { 1, 3, 6 } },
            { 5, new int[] { 2, 7 } },
            { 6, new int[] { 4, 8 } },
            { 7, new int[] { 5, 8, 9 } },
            { 8, new int[] { 6, 7, 9 } },
            { 9, new int[] { 7, 8 } },
        };

        public string TargetPosition { get; } = "1230456789";

        public string GetNewPosition(string position, char move)
        {
            var indexOfMove = position.IndexOf(move);
            var indexOfZero = position.IndexOf("0");

            var result = new StringBuilder(position);
            result[indexOfZero] = move;
            result[indexOfMove] = '0';
            return result.ToString();
        }

        public char[] GetPossibleMoves(string position)
        {
            var indexOfZero = position.IndexOf('0');
            var indexes = _edges[indexOfZero];
            return indexes.Select(n => position[n]).ToArray();
        }
    }
}
