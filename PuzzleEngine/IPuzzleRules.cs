﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleEngine
{
    public interface IPuzzleRules
    {
        string TargetPosition { get; }
        char[] GetPossibleMoves(string position);
        string GetNewPosition(string position, char move);
    }
}
