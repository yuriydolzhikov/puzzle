﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleEngine
{
    public interface IResolver
    {
        int[] Solve(int[] input);
    }
}
