﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleEngine
{
    public struct Node
    {
        public int Rank { get; private set; }
        public char Move { get; private set; }

        public Node(int _rank, char _move)
        {
            Rank = _rank;
            Move = _move;
        }
    }
}
