﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleEngine
{
    public class Resolver: IResolver
    {
        private IPuzzleRules _rules;
        private Dictionary<string, Node> _graph = new Dictionary<string, Node>();
        private const string GraphFileName = @"..\..\..\graph.csv";

        public Resolver(IPuzzleRules rules)
        {
            _rules = rules;
        }

        public void InitGraph(bool caching)
        {
            // CreateGraph() takes about 5 minutes, so caching it to file
            if (caching)
            {
                if (!LoadGraph())
                {
                    CreateGraph();
                    SaveGraph();
                }
            }
            else
            {
                CreateGraph();
            }
        }

        public int[] Solve(int[] input)
        {
            var position = string.Join("", input.Select(n => n.ToString()));
            return Solve(position);
        }

        public Dictionary<string, Node> GetGraph() => _graph;

        private int[] Solve(string startPosition)
        {
            var result = new List<int>();
            var position = startPosition;

            var node = _graph[position];
            if (node.Rank == 0)
            {
                return new int[0];
            }

            while (node.Rank > 0)
            {
                result.Add(Convert.ToInt32(node.Move.ToString()));
                position = _rules.GetNewPosition(position, node.Move);
                node = _graph[position];
            }

            return result.ToArray();
        }

        private void CreateGraph()
        {
            _graph[_rules.TargetPosition] = new Node(0, '0');
            CreateGraph(_rules.TargetPosition);
        }

        private bool LoadGraph()
        {
            try
            {
                var lines = File.ReadAllLines(GraphFileName);
                foreach (var line in lines)
                {
                    var splitted = line.Split(',');
                    var node = new Node(Convert.ToInt32(splitted[1]), Convert.ToChar(splitted[2]));
                    _graph[splitted[0]] = node;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void SaveGraph()
        {
            var lines = _graph.Keys.Select(key => $"{key},{_graph[key].Rank},{_graph[key].Move}");
            File.WriteAllLines(GraphFileName, lines);
        }

        private void CreateGraph(string startPosition)
        {
            var notProcessed = new List<string> { startPosition };
            while (notProcessed.Count > 0)
            {
                var position = notProcessed[0];
                var node = _graph[position];
                var rank = node.Rank;
                var previousMove = _graph[position].Move;

                var moves = _rules.GetPossibleMoves(position);
                foreach (var move in moves)
                {
                    if (move == previousMove) continue;
                    var newPosition = _rules.GetNewPosition(position, move);
                    if (!_graph.ContainsKey(newPosition))
                    {
                        _graph[newPosition] = new Node(rank+1, move);
                        notProcessed.Add(newPosition);
                    }
                }
                notProcessed.RemoveAt(0);
            }
        }
    }
}
