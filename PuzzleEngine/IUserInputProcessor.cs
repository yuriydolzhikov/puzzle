﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleEngine
{
    public interface IUserInputProcessor
    {
        bool InputIsValid(string input);
        int[] ConvertInput(string input);
        string ConvertOutput(int[] output);
    }
}
